<?php
  ob_start();
  include('php/conexion.php');
  $consulta = $conexion->query("SELECT * FROM personas");

?>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Curso PHP - Eliminar</title>
    <?php include('php/head.php'); ?>
  </head>
  <body>
    <div class="container">
      <a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#crear">Crear persona</a>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Edad</th>
            <th scope="col">Editar</th>
          </tr>
        </thead>
        <tbody>
          <?php while ($tabla1 = mysqli_fetch_array($consulta)) {
          ?>
          <tr>
            <th scope="row"><?php echo $tabla1['id']; ?></th>
            <td><?php echo $tabla1['nombre']; ?></td>
            <td><?php echo $tabla1['apellido']; ?></td>
            <td><?php echo $tabla1['edad']; ?></td>
            <td>
              <a href="funcion-e.php?eliminar=<?php echo $tabla1['id']; ?>" class="btn btn-sm btn-danger">Eliminar</a>
              <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#a<?php echo $tabla1['id']; ?>">Editar</a>
              <!-- Modal -->
              <div class="modal fade" id="a<?php echo $tabla1['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Editar | <?php echo $tabla1['nombre']; ?></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form class="" method="post">
                        <div class="row">
                          <div class="col">
                            <input type="text" name="id" value="<?php echo $tabla1['id']; ?>" hidden>
                            <input name="nombre" type="text" class="form-control" placeholder="Nombre" value="<?php echo $tabla1['nombre']; ?>">
                          </div>
                          <div class="col">
                            <input name="apellido" type="text" class="form-control" placeholder="Apellido" value="<?php echo $tabla1['apellido']; ?>">
                          </div>
                        </div>
                        <br>
                        <input name="edad" type="text" class="form-control" placeholder="Edad" value="<?php echo $tabla1['edad']; ?>">
                        <br>
                        <button name="boton" type="submit" class="btn btn-primary btn-block">Editar</button>
                        <?php
                        if (isset($_POST['boton'])) {
                          $conexion->query("UPDATE personas SET nombre='".$_POST['nombre']."',apellido='".$_POST['apellido']."', edad='".$_POST['edad']."' WHERE id='".$_POST['id']."'");
                          header("Location: crear.php");
                        }

                         ?>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </body>
  <footer>
    <?php include('php/footer.php'); ?>
  </footer>
  <!-- Modal -->
  <div class="modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Crear persona</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="" method="post">
            <div class="row">
              <div class="col">
                <input name="nombre2" type="text" class="form-control" placeholder="Nombre" value="">
              </div>
              <div class="col">
                <input name="apellido2" type="text" class="form-control" placeholder="Apellido" value="">
              </div>
            </div>
            <br>
            <input name="edad2" type="text" class="form-control" placeholder="Edad" value="">
            <br>
            <button name="btn-crear" type="submit" class="btn btn-primary btn-block">Crear</button>
            <?php
              if (isset($_POST['btn-crear'])) {
                $conexion->query("INSERT INTO personas (nombre, apellido, edad) VALUES ('".$_POST['nombre2']."','".$_POST['apellido2']."','".$_POST['edad2']."')");
                header("Refresh: 1; URL=crear.php");
              }
             ?>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</html>
