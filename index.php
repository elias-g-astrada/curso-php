<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Curso PHP</title>
    <?php include('php/head.php'); ?>
  </head>
  <body>
    <div class="container">
      <div class="row mt-5">
        <div class="col-md-4">
          <div class="card">
            <div class="card-header card-primary">
              Consulta
            </div>
            <div class="card-body">
              <a href="crear.php" class="btn btn-primary">Acceder</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
  <footer>
    <?php include('php/footer.php'); ?>
  </footer>
</html>
