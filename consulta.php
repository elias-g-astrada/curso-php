<?php include('php/conexion.php'); ?>
<?php $consulta = $conexion->query("SELECT * FROM personas");
?>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Curso PHP - Consulta</title>
    <?php include('php/head.php'); ?>
  </head>
  <body>
    <div class="container">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Edad</th>
          </tr>
        </thead>
        <tbody>
          <?php while ($tabla1 = mysqli_fetch_array($consulta)) {
          ?>
          <tr>
            <th scope="row"><?php echo $tabla1['id']; ?></th>
            <td><?php echo $tabla1['nombre']; ?></td>
            <td><?php echo $tabla1['apellido']; ?></td>
            <td><?php echo $tabla1['edad']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </body>
  <footer>
    <?php include('php/footer.php'); ?>
  </footer>
</html>
